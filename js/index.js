$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();

    $("#carousel").carousel({
        interval: 2000
    });

    $("#contact").on('show.bs.modal', function (e){
        $("[data-toggle='modal']").removeClass('btn-outline-success');
        $("[data-toggle='modal']").addClass('btn-warning');
        $("[data-toggle='modal']").prop('disabled', true);
    });

    $("#contact").on('hidden.bs.modal', function (e){
        $("[data-toggle='modal']").removeClass('btn-warning');
        $("[data-toggle='modal']").addClass('btn-outline-success');
        $("[data-toggle='modal']").prop('disabled', false);
    });

});